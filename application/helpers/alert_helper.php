<?php

/**
 * -------------------------------------------------------------------
 * Developed and maintained by SatrioDwiPrabowo
 * -------------------------------------------------------------------
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('message_box')) {

    function message_box($message_type, $close_button = TRUE)
    {
        $CI = &get_instance();
        $message = $CI->session->flashdata($message_type);
        $retval = '';

        if ($message) {
            switch ($message_type) {
                case 'success':
                    $retval = '<script type="text/javascript">
                              $(function(){
                                var animate_in = $("#animate_in").val(),
                                animate_out = $("#animate_out").val();
                                new PNotify({
                                  title: "Berhasil !",
                                  text: " '.$message.' ",
                                  type: "success",
                                  styling: "bootstrap3",
                                  animate: {
        animate: true,
        in_class: "bounceInLeft",
        out_class: "slideOutUp"
    }
                                });
                              });
                              </script>';
                    break;
                case 'error':
                    $retval = '<script type="text/javascript">
                              $(function(){
                                new PNotify({
                                  title: "Gagal !",
                                  text: " '.$message.' ",
                                  type: "error",
                                  styling: "bootstrap3"
                                });
                              });
                              </script>';
                    break;
                case 'info':
                    $retval = '<script type="text/javascript">
                              $(function(){
                                new PNotify({
                                  title: "Informasi !",
                                  text: " '.$message.' ",
                                  type: "info",
                                  styling: "bootstrap3"
                                });
                              });
                              </script>';
                    break;
                case 'warning':
                    $retval = '<script type="text/javascript">
                              $(function(){
                                new PNotify({
                                  title: "Pemberitahuan !",
                                  text: " '.$message.' ",
                                  type: "warning",
                                  styling: "bootstrap3"
                                });
                              });
                              </script>';
                    break;
            }
            return $retval;
        }
    }

}

if (!function_exists('set_message')) {

    function set_message($type, $message)
    {
        $CI = &get_instance();
        $CI->session->set_flashdata($type, $message);
    }

}



