<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('template/page_header'); ?>
<body class="nav-md">
    <?php $this->load->view("template/page_menu"); ?>
<?php $this->load->view('template/page_top'); ?>
 <!-- page content -->
        <div class="right_col" role="main">
<?php echo $subview ?>
</div>
<?php $this->load->view('template/page_footer'); ?>    